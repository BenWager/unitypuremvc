#Unity PureMVC Template#

A modular approach to setting up a Unity / PureMVC project.

The code is split in to 3 distinct parts:

* Core
* Application
* Modules

They can be pulled in to an empty Unity repositry as Submodules. (Although the Application branch should have it's submodule reference removed after importing). More on that later.


**Core**

This is the part of the code base that Connects Unity to PureMVC, initializes the application code, and initializes any modules.
It is unlikely the Core codebase will ever need to be updated for a particular project


**Application**

This is where YOUR application code will lie. The application branch of this repository is an example. 
You can copy in to your project to use it as a base.
This is where you can create Mediators to control any modules you use, and implement additional application features.


**Modules**

The modules are extensions to the core that implement specific features. 
For instance, the included application branch implements the CameraSystem, which allows you to transition a camera to a specific location, with Complete callbacks etc.
The DataSystem allows you to read in a data file, for example a .json of USP data from the reasources folder, or externally, and can handle storing the resulting data in an ApplicationDataVO.
MenuSystem lets you easily create a menu, you can use custom menuItem templates etc.



##Installation##


**Step 1**

* Create an empty repository and clone to your local machine

**Step 2**

* Create a blank Unity Project in the repository


**Step 3**

* Create a submodule for Core
	* Source path: https://BenWager@bitbucket.org/BenWager/unitypuremvc.git
	* Local path: path_to_Unity_project/Assets/INDG/Core
	* Source branch: core


**Step 4**

* Create submodule for Application (this is just to copy application code, we will manually delete submodule reference later)
	* Source path: https://BenWager@bitbucket.org/BenWager/unitypuremvc.git
	* Local path: path_to_Unity_project/Assets/INDG/Application
	* Source branch: application


**Step 5**

* Create submodule for DataSystem (used by application example)
	* Source path: https://BenWager@bitbucket.org/BenWager/unitypuremvc.git
	* Local path: path_to_Unity_project/Assets/INDG/Modules/DataSystem
	* Source branch: modules/DataSystem
* Create submodule for CameraSystem (used by application example)
	* Source path: https://BenWager@bitbucket.org/BenWager/unitypuremvc.git
	* Local path: path_to_Unity_project/Assets/INDG/Modules/CameraSystem
	* Source branch: modules/CameraSystem


**Step 6**

* Remove application submodule reference
	* mv yoursubmodule yoursubmodule_tmp
	* git submodule deinit yourSubmodule
	* git rm yourSubmodule
	* mv yoursubmodule_tmp yoursubmodule
	* manually delete .git from yoursubmodule
	* git add yoursubmodule

**Step 7**

* Hook Unity with Core
	* Create directory path_to_Unity_project/Assets/Scenes	
	* Create Scene called Main
	* Create Scene called Environment
	* Add both scenes to Build settings
	* Open Main scene
	* Delete all GameObjects
	* Create Empty GameObject
	* Add component "CoreBehaviour"
	* Run Scene

After following these steps and running the Scene, you should see that the Main scene has loaded the Environment scene, created a CameraSystem GameObject and EventSystem object

New Proxies and Mediators can be registered in INDG.Application.Controller.Commands.ApplicationPrepareViewCommand